# -*- mode: ruby -*-
# vi: set ft=ruby :
#####################################################
# __      __                     .__                #
#/  \    /  \_____ _______  ____ |__| ____    ____  #
#\   \/\/   /\__  \\_  __ \/    \|  |/    \  / ___\ #
# \        /  / __ \|  | \/   |  \  |   |  \/ /_/  >#
#  \__/\  /  (____  /__|  |___|  /__|___|  /\___  / #
#       \/        \/           \/        \//_____/  #
#                                                   #
# Due to the submodule's useage of symbolic links   #
# the vagrant box will not work correctly on        #
# NTFS formatted host machines.                     #
#####################################################
Vagrant.configure(2) do |config|

  module OS
    def OS.windows?
        (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
    end

    def OS.mac?
        (/darwin/ =~ RUBY_PLATFORM) != nil
    end

    def OS.unix?
        !OS.windows?
    end

    def OS.linux?
        OS.unix? and not OS.mac?
    end
  end

  config.vm.box = "precise32"

  config.vm.box_url = "http://files.vagrantup.com/precise32.box"

  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.vm.define "nyu-stern" do |node|
    node.vm.hostname = "www.stern.nyu.npvm"
    node.vm.network :private_network, ip: "172.22.22.23"
    node.hostmanager.aliases = [ "portal-stern.nyu.npvm m.stern.local" ]
  end
  config.vm.provision :hostmanager

  is_windows_host = "#{OS.windows?}"
  puts "is_windows_host: #{OS.windows?}"
  if OS.windows?
    puts "Vagrant launched from windows."
    # SMB synced folders are never pruned or cleaned up. Vagrant never removes
    # it. To clean up SMB synced folder shares, periodically run `net share` in
    # a command prompt, find the shares you don't want, then run
    # `net share NAME /delete` for each, where NAME is the name of the share.
    config.vm.synced_folder "./project", "/var/www/html/", type: "smb"
   elsif OS.mac?
    puts "Vagrant launched from mac."
    config.vm.synced_folder "./project", "/var/www/html/", type: "nfs"
   elsif OS.unix?
    puts "Vagrant launched from unix."
    config.vm.synced_folder "./project", "/var/www/html/", type: "nfs"
   elsif OS.linux?
    puts "Vagrant launched from linux."
    config.vm.synced_folder "./project", "/var/www/html/", type: "nfs"
  else
    puts "Vagrant launched from mac/unix/linux/unknown platform."
    config.vm.synced_folder "./project", "/var/www/html/", type: "nfs"
  end

  config.vm.provider "virtualbox" do |vm|
    vm.customize ["modifyvm", :id, "--memory", "3072"]
    vm.customize ["modifyvm", :id, "--cpus", "1"]
    vm.customize ["modifyvm", :id, "--cpuexecutioncap", "90"]
  end

  config.vm.provision "shell" do |script|
    script.path = "project/scripts/provisioner.sh"
  end


end
