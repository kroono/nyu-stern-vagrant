#! /bin/bash
echo "Backing up database..."

# Backup portal
echo "...portal."
mysqldump -uroot -proot portal > /var/www/html/setup/databases/portal.sql

# Backup stern
echo "...stern."
mysqldump -uroot -proot nyustern > /var/www/html/setup/databases/dump.sql
